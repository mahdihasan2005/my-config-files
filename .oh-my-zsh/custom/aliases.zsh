# my custom aliases
alias cat='batcat'
alias l='exa -a --icons --group-directories-first'
alias lt='exa -aT --icons --group-directories-first'
alias ll='exa -al --icons --group-directories-first'
alias llt='exa -alT --icons --group-directories-first'
alias rm='rm -v'
alias vim='nvim'
alias python='python3'
