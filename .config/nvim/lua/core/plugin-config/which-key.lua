local wk = require("which-key")
wk.register({
    ["<leader>b"] = {
        name = "+Buffers",
        d = { ":bdelete<cr>", "Delete Buffer" },
        n = { ":bnext<cr>", "Next Buffer" },
        p = { ":bprevious<cr>", "Previous Buffer" },
    },
    ["<leader>f"] = {
        name = "+File",
        f = { ":Telescope find_files<cr>", "Find File" },
        r = { ":Telescope oldfiles<cr>", "Open Recent File" },
        n = "New File",
    },
    ["<leader>g"] = {
        name = "+Git",
        s = { ":!git status<cr>", "Status" },
        l = { ":term lazygit<cr>", "Lazygit" },
    },
    ["<leader>l"] = {
        name = "+LSP",
        f = { ":LspZeroFormat<cr>", "Format" },
        s = { ":Telescope lsp_document_symbols<cr>", "Document Symbols" },

    },
    ["<leader>p"] = {
        name = "+Packer",
        s = { ":PackerSync<cr>", "Sync" },
    },
    ["<leader>r"] = { ":Telescope oldfiles<cr>", "Open Recent File" },
})
