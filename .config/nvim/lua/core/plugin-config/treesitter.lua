require'nvim-treesitter.configs'.setup {
    -- A list of all parser names, or "all"
    ensure_installed = { "c", "lua", "python", "vim" },

    -- Install parser synchronously ( only applied to `ensure_installed`)
    sync_install = true,
    auto_install = true,
    highlight = {
        enable = true,
    },
}
