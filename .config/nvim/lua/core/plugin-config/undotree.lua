-- vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)

require("which-key").register({
    ["<leader>u"] = { "vim.cmd.UndotreeToggle", "Undo Tree" },
})
