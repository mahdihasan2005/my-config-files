vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

require("nvim-tree").setup()

-- vim.keymap.set('n', '<c-n>', ':NvimTreeFindFileToggle<CR>')

require("which-key").register({
    ["<leader>n"] = { ":NvimTreeFindFileToggle<cr>", "Nvim Tree" },
})
