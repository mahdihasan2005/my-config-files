-- Set leader key to space
vim.g.mapleader = ' '

-- Use <leader>w to save file
vim.keymap.set("n", "<leader>w", ":w<cr>")

-- Copying from ThePrimeagen
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- greatest remap ever
vim.keymap.set("x", "<leader>p", [["_dP]])

-- next greatest remap ever : asbjornHaland
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

-- run code with F5
vim.api.nvim_set_keymap('n', '<F5>', [[:term code-runner % <CR>]], { noremap = true, silent = true })

-- buffer navigation
vim.keymap.set("n", "<leader>bn", ":bnext") -- go to next buffer
vim.keymap.set("n", "<leader>bp", ":bprev") -- go to previous buffer
vim.keymap.set("n", "<leader>bd", ":bdel") -- delete current buffer
